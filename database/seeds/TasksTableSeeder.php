<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task = new \App\Task();
        $task->done = false;
        $task->title = "Task 1";
        $task->description = "Task 1 description";
        $task->due_date = "2017-10-10";
        $task->user_id = 1;

        $task->save();
        $task = new \App\Task();
        $task->done = true;
        $task->title = "Task 2";
        $task->description = "Task 2 description";
        $task->due_date = "2017-10-10";
        $task->user_id = 1;
        $task->save();

        $task = new \App\Task();
        $task->done = false;
        $task->title = "Task 3";
        $task->description = "Task 3 description";
        $task->due_date = "2017-10-10";
        $task->user_id = 2;
        $task->save();
    }
}
