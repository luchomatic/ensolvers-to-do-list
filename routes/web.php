<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Task;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/delete/{id}', function ($id) {
    $task = Task::find($id);
    if($task->user_id == Auth::id()){
        //is the owner of the task
        $task->delete();
        return ("true");
    }else{
        //is NOT the owner of the task
        return ("false");
    }
});

Route::post('/edit/{id}', function ($id) {
    $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $task = Task::find($id);
    $done = 0;
    if (isset($_POST['done'])){
        $done = ($_POST['done'] == "on") ? 1 : 0;
    }
    $due_date = (isset($_POST['date'])) ? $_POST['date']: date('Y-m-d H:i:s');;
    if($task->user_id == Auth::id()){
        //is the owner of the task
        $task->description = isset($_POST['description']) ? $_POST['description'] : $task->description;
        $task->done = $done;
        $task->due_date = $due_date;
        $task->title = isset($_POST['title']) ? $_POST['title'] : $task->title;
        $task->save();
        return ("true");
    }else{
        //is NOT the owner of the task
        return ("false");
    }
});

Route::post('/add', function () {
    $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $task = new Task;
    $done = 0;
    if (isset($_POST['done'])){
        $done = ($_POST['done'] == "on") ? 1 : 0;
    }
    $due_date = (isset($_POST['date'])) ? $_POST['date']: date('Y-m-d H:i:s');;
    if(Auth::id()){
        //is auth
        $task->description = isset($_POST['description']) ? $_POST['description'] : $task->description;
        $task->done = $done;
        $task->user_id = Auth::id();
        $task->due_date = $due_date;
        $task->title = isset($_POST['title']) ? $_POST['title'] : $task->title;
        $task->save();
        return redirect('/home');
    }else{
        //is NOT logged in
        return ("false");
    }
});