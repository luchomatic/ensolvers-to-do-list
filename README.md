# README #

This app was developed using Laravel 5, it took 5 hours of development and documentation and here are the steps to run it

### How to install and run ###

* Checkout the repository
* run the command composer install on the root directory of the app
* Create a .env file on the root directoru and change the variables so they can connect your database (you can find an example on the root of the repository called example.env)
* run the command php artisan key:generate to create an app key
* create a database with the name that you used on the .env file on the DB_DATABASE field
* run the command php artisan migrate --seed (the argument --seed will add some default values/users to the database)
* run the command php artisan serve
* go to: http://localhost:8000/login
* In order to login you can use the user luciano@inudev.com password: secret to review some existing tasks