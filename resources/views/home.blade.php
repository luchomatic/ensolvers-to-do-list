@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>To do list</h1>
                    @foreach ($tasks as $task)
                        @if ($task->done)
                            <div class="task green" >
                        @else
                            <div class="task" >
                        @endif
                        <span>{{ $task->title }}</span>
                        <span>
                        <a href="#delete-popup-{{$task->id}}" class="open-popup-link list-icon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </span>
                            <div id="delete-popup-{{$task->id}}" class="white-popup mfp-hide">
                                <p>Are you sure you want to delete this task: "{{$task->title}}"?</p>
                                <button class="delete-task" data-id="{{$task->id}}">Yes</button>
                                <button class="mfp-close">No</button>
                            </div>

                            <span>
                            <a href="#edit-popup-{{$task->id}}" class="open-popup-link list-icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        </span>
                            <div id="edit-popup-{{$task->id}}" class="white-popup mfp-hide">
                                <h2>Editing task: {{$task->title}}</h2>

                                <form id="edit-form-{{$task->id}}">
                                    <label for="title">Title</label><br>
                                    <input id="title" name="title" type="text" value="{{$task->title}}"><br>

                                    <label for="date">Date</label><br>
                                    <input id="date" name="date" type="date" value="{{$task->due_date}}"><br>

                                    <label for="description">Description</label><br>
                                    <textarea id="description" name="description">{{$task->description}}</textarea><br>
                                    <label for="done">Done</label><br>
                                    @if ($task->done)
                                        <input name="done" type="checkbox" id="done" checked><br>
                                    @else
                                        <input name="done" type="checkbox" id="done"><br>
                                    @endif
                                    {{ csrf_field() }}
                                    <input type="submit" class="edit-task" data-id="{{$task->id}}" value="Save changes">
                                    <button type="button" class="mfp-close">Discard changes</button>
                                </form>

                            </div>
                        </div>
                        @endforeach
                        <span>
                            <a id="add-popup-button" href="#add-popup" class="open-popup-link">Create new to-do item</a>
                        </span>
                        <div id="add-popup" class="white-popup mfp-hide">
                            <h2>Adding new task</h2>

                            <form id="add-form" method="post" action="/add">
                                <label for="title">Title</label><br>
                                <input id="title" name="title" type="text" value=""><br>

                                <label for="date">Date</label><br>
                                <input id="date" name="date" type="date" value="{{$task->due_date}}"><br>

                                <label for="description">Description</label><br>
                                <textarea id="description" name="description"></textarea><br>
                                <label for="done">Done</label><br>

                                <input name="done" type="checkbox" id="done"><br>
                                {{ csrf_field() }}
                                <input type="submit" class="add-task" value="Add task">
                                <button type="button" class="mfp-close">Close</button>
                            </form>

                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
