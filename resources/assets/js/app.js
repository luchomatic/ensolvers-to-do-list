
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',
    components: {

    }
});


jQuery(document).ready(function($) {
    jQuery('.open-popup-link').magnificPopup({
        type:'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });

    $('.delete-task').on('click', function (){
        var taskId = $(this)[0].dataset.id;
        $.ajax({url: "/delete/" +taskId, success: function(result){
            if (result === "true"){
                window.location = "/home";
            }else{
                alert("There was an error while deleting the task");
            }
        }});
    });

    $('.edit-task').on('click', function (){

        var taskId = $(this)[0].dataset.id;

        $("#edit-form-" + taskId).submit(function(e) {

            var url = "/edit/" + taskId;
            console.log(url);
            $.ajax({
                type: "POST",
                url: url,
                data: $("#edit-form-" + taskId).serialize(), // serializes the form's elements.
                success: function(data)
                {
                    if (data === "true"){
                        window.location = "/home";
                    }else{
                        alert("There was an error while editing the task");
                    }
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    });

});

